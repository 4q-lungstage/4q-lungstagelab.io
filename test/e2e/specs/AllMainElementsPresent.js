// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'All Main view elements are present': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js

    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('#layout', 5000)
      .assert.elementPresent('#side-bar')
      .assert.elementPresent('#main-editor')
      .assert.elementPresent('#menu-bar')
      .assert.elementPresent('#patient-info')
      .end()
  },
  'right bar is initially not visible': function (browser) {
    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('#layout', 5000)
      .assert.elementNotPresent('#right-bar')
      .end()
  }
}
