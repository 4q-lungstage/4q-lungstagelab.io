// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Selecting first result works': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js

    browser
      .url(browser.globals.devServerURL)
      .waitForElementVisible('#layout', 5000)
      .assert.containsText('.results > div:first-child h2', 'Primärtumor (1)')
      .assert.containsText('.results > div:first-child article.staging-result-selector', 'Kategorie T4')
      .click('.results > div:first-child article.staging-result-selector')
      .waitForElementVisible('#main-editor', 5000)
      .assert.containsText('#main-editor article:first-child .controls .dropdown-text', '0003_ct_soft_axial')
      .assert.elementPresent('#right-bar')
      .end()
  }
}
