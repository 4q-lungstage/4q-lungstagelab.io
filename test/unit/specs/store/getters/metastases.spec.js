import store from '@/store'

describe('metastases', () => {
  it('has only primary tumor element', () => {
    const metastases = store.getters.metastases
    expect(metastases).toHaveLength(0)
  })
})
