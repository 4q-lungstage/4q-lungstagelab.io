import store from '@/store'
import { find } from 'lodash'

describe('getSliceImage', () => {
  it('gets the right image', () => {
    store.dispatch('selectResult', 1)
    const image = find(store.state.selectedExamination.slices, { id: store.state.selectedResult.sliceId }).images[0]
    expect(store.getters.getSliceImage(0)).toBe(image)
  })
})
