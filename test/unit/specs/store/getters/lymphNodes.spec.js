import store from '@/store'

describe('lymphNodes', () => {
  it('has only primary tumor element', () => {
    const lymphNodes = store.getters.lymphNodes
    expect(lymphNodes).toHaveLength(3)
    lymphNodes.forEach(node => {
      expect(node.type).toEqual('lymphNode')
    })
  })
})
