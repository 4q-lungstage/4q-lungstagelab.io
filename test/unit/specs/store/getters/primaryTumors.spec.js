import store from '@/store'

describe('primaryTumors', () => {
  it('has only primary tumor element', () => {
    const primaryTumors = store.getters.primaryTumors
    expect(primaryTumors).toHaveLength(1)
    expect(primaryTumors[0].type).toEqual('primaryTumor')
  })
})
