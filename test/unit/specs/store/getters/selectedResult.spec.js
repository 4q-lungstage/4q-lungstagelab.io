import store from '@/store'

describe('selectedResult', () => {
  it('is the selected result', () => {
    expect(store.getters.selectedResult).toEqual({})
    const testResult = store.state.patient.examinations[0].results[0]
    store.dispatch('selectResult', testResult.id)
    expect(store.getters.selectedResult).toBe(testResult)
  })
})
