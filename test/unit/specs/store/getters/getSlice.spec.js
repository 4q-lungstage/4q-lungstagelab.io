import store from '@/store'
import { find } from 'lodash'

describe('getSlice', () => {
  it('returns the right slice', () => {
    const t4 = find(store.state.patient.examinations[0].slices, { id: 't4_selected' })
    expect(store.getters.getSlice('t4_selected').id).toEqual('t4_selected')
    expect(store.getters.getSlice('t4_selected')).toBe(t4)
  })
})
