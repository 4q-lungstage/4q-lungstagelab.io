import store from '@/store'

const ctViewerOne = store.state.ctViewers[0]

describe('setSelectedResult', () => {
  it('viewer tool gets activated', () => {
    expect(ctViewerOne.tools[0].active).toBeFalsy()
    expect(ctViewerOne.tools[1].active).toBeFalsy()
    expect(ctViewerOne.tools[2].active).toBeFalsy()
    expect(ctViewerOne.tools[3].active).toBeFalsy()

    store.commit('toggleViewerTool', { viewerId: 0, toolId: 0 })

    expect(ctViewerOne.tools[0].active).toBeTruthy()
    expect(ctViewerOne.tools[1].active).toBeFalsy()
    expect(ctViewerOne.tools[2].active).toBeFalsy()
    expect(ctViewerOne.tools[3].active).toBeFalsy()
  })

  it('toggling another tool on the same viewer deactivates other active tools', () => {
    store.commit('toggleViewerTool', { viewerId: 0, toolId: 1 })

    expect(ctViewerOne.tools[0].active).toBeFalsy()
    expect(ctViewerOne.tools[1].active).toBeTruthy()
    expect(ctViewerOne.tools[2].active).toBeFalsy()
    expect(ctViewerOne.tools[3].active).toBeFalsy()
  })

  it('toggling the same tool deactivates it', () => {
    store.commit('toggleViewerTool', { viewerId: 0, toolId: 1 })

    expect(ctViewerOne.tools[0].active).toBeFalsy()
    expect(ctViewerOne.tools[1].active).toBeFalsy()
    expect(ctViewerOne.tools[2].active).toBeFalsy()
    expect(ctViewerOne.tools[3].active).toBeFalsy()
  })
})
