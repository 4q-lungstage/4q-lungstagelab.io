import store from '@/store'

const [targetResultOne, targetResultTwo] = store.state.patient.examinations[0].results

describe('setSelectedResult', () => {
  it('selects result if none selected', () => {
    expect(store.state.selectedResult).toEqual({})
    store.commit('setSelectedResult', 1)
    expect(store.state.selectedResult).toBe(targetResultOne)
    expect(store.state.selectedResult.selected).toBe(true)
    expect(targetResultOne.selected).toBe(true)
  })

  it('selecting another result deselects prior result', () => {
    store.commit('setSelectedResult', 2)
    expect(store.state.selectedResult).toBe(targetResultTwo)
    expect(store.state.selectedResult.selected).toBe(true)
    expect(targetResultTwo.selected).toBe(true)
    expect(targetResultOne.selected).toBe(false)
  })
})
