const getDefaultOrInstall = (result) => {
  return result.default || result.install
}

export default (importArray) => importArray.map(
  imported => imported.then(getDefaultOrInstall)
)
