export default (importsArray, thenFunction) => Promise.all(importsArray).then(thenFunction)
