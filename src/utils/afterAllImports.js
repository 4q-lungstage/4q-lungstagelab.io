import { afterAll, asyncImport } from './index'

export default (importsArray, thenFunction) => {
  afterAll(asyncImport(importsArray), thenFunction)
}
