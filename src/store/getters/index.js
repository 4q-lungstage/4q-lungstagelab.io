import { filter, find, values, mapValues } from 'lodash'

import patientData from './patientData'

export default {
  patientData,

  primaryTumors: (state) => filter(state.selectedExamination.results, { display: true, type: 'primaryTumor' }),

  lymphNodes: (state) => filter(state.selectedExamination.results, { display: true, type: 'lymphNode' }),

  metastases: (state) => filter(state.selectedExamination.results, { display: true, type: 'metastasis' }),

  selectedResult: (state) => state.selectedResult,

  storyIs: ({ storyState: { value } }) => (storyQuestion) => (value === storyQuestion),

  storyIsNot: ({ storyState: { value } }) => (storyQuestion) => (value !== storyQuestion),

  getSlice: (state) => (sliceId) => find(state.selectedExamination.slices, { id: sliceId }),

  getSliceImage: (state) => (viewerId) => {
    if (state.selectedResult.sliceId) {
      return find(state.selectedExamination.slices, { id: state.selectedResult.sliceId }).images[viewerId]
    }
    return null
  },

  getViewer: (state) => (viewerId) => state.ctViewers[viewerId],

  getTool: (state) => (viewerId, key) => find(state.ctViewers[viewerId].tools, { key }),

  getResultBySliceId: (state) => (sliceId) => find(state.selectedExamination.results, { sliceId }),

  propertiesChecked: (state) => (propertyKey) => {
    const mappedProperties = mapValues(state.features.properties, (text, key) => ({ key, text, checked: false }))
    state.selectedResult.properties.forEach(property => {
      mappedProperties[property].checked = true
    })
    return values(mappedProperties)
  },

  getCurrentLines: (state) => (viewerId) => {
    if (!state.selectedResult) { return null }
    return state.selectedResult.lines[viewerId]
  }
}
