import {
  toLocaleDate
} from '../../utils'

export default ({
  selectedExamination: {
    id: examinationId,
    date,
    type,
    series,
    staging,
    stage,
    confidence,
    adapted
  },
  patient: {
    name,
    preName,
    birthDate,
    id
  },
  locale
}) => ({
  id,
  name,
  preName,
  birthDate: toLocaleDate(birthDate, locale),
  examination: {
    id: examinationId,
    adapted,
    type,
    series,
    staging,
    stage,
    date: toLocaleDate(date, locale),
    confidence: confidence * 100
  }
})
