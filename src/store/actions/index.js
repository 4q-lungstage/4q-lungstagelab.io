import delay from 'delay'

export default {
  selectResult: ({
    commit
  }, id) => {
    commit('setStoryState', 'results')
    commit('setSelectedResult', id)
  },

  selectRightBarDropdownOption: ({
    commit
  }, {
    dropDownId,
    optionId
  }) => {
    commit('selectRightBarDropDownOption', {
      dropDownId,
      optionId
    })
    commit('toggleRightBarDropDown', dropDownId)
  },

  switchToScrolledStoryState: ({
    commit
  }) => {
    commit('setStoryState', 'scrolled')
    commit('setSelectedResult', 5)
  },

  setNewMarkedResultActive: ({
    commit
  }) => {
    commit('toggleCalculatingCircle')
    delay(3500).then(() => {
      commit('toggleCalculatingCircle')
      commit('mutateNewResultActive')
      commit('toggleExaminationAdapted')
    })
  }
}
