import { find } from 'lodash'

const setResult = (state, id) => {
  state.selectedResult = find(state.selectedExamination.results, { id })
  state.selectedResult.selected = true
}

export default (state, resultId) => {
  if (!state.selectedResult) {
    return setResult(state, resultId)
  }
  if (state.selectedResult.id === resultId) { return }
  state.selectedResult.selected = false
  setResult(state, resultId)
}
