import {
  last,
  find
} from 'lodash'

import setSelectedResult from './setSelectedResult'

export default {
  setSelectedResult,

  changeResultFeature: (state, {
    featureKey,
    featureValue
  }) => {
    state.selectedResult[featureKey] = featureValue
  },

  toggleRightBarDropDown: (state, feature) => {
    if (state.rightBar.dropDownIsOpen === feature) {
      state.rightBar.dropDownIsOpen = null
      return
    }
    state.rightBar.dropDownIsOpen = feature
  },

  setStoryState: (state, newStoryState) => {
    state.storyState.value = newStoryState
    if (newStoryState === 'initial' && state.selectedResult.selected) {
      state.selectedResult.selected = false
      state.selectedResult = {}
    }
  },

  toggleViewerTool: (state, {
    viewerId,
    toolId
  }) => {
    const tools = state.ctViewers[viewerId].tools
    if (tools[toolId].active) {
      tools[toolId].active = false
    } else {
      tools.map(tool => {
        tool.active = false
      })
      tools[toolId].active = true
    }
  },

  setScrollViewerImageShift: (
    state, {
      viewerId,
      imageShift: {
        x,
        y,
        xTotal,
        yTotal
      }
    }
  ) => {
    state.ctViewers[viewerId].imageShift.x = typeof x === 'number' ? x : 0
    state.ctViewers[viewerId].imageShift.y = typeof y === 'number' ? y : 0
    state.ctViewers[viewerId].imageShift.xTotal =
      typeof xTotal === 'number' ? xTotal : 0
    state.ctViewers[viewerId].imageShift.yTotal =
      typeof yTotal === 'number' ? yTotal : 0
  },

  switchLocale: state => locale => {
    state.locale = locale
  },
  switchToGerman: state => {
    state.locale = 'de-CH'
  },
  switchToEnglish: state => {
    state.locale = 'en-GB'
  },

  toggleCalculatingCircle: state => {
    state.calculateing = !state.calculateing
  },

  toggleExaminationAdapted: state => {
    state.selectedExamination.adapted = !state.selectedExamination.adapted
  },

  mutateNewResultActive: state => {
    const result = last(state.selectedExamination.results)
    const slice = find(state.selectedExamination.slices, {
      id: result.sliceId
    })
    slice.images[0].drawingSubmitted = true
    slice.images[0].drawing = false
    result.display = true
  }
}
