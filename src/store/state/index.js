import menuGroups from './menuGroups.yaml'
import ctViewers from './ctViewers.yaml'
import results from './results.yaml'
import slices from './slices.yaml'
import state from './state.yaml'

state.patient.examinations[0].results = results
state.patient.examinations[0].slices = slices
state.selectedExamination = state.patient.examinations[0]
state.magnifyFirst = ctViewers[0].tools[2]

export default {
  ...state,
  menuGroups,
  ctViewers
}
