import Layout from '@/components/Layout'

export default [
  {
    path: '/',
    name: 'Layout',
    component: Layout
  }
]
