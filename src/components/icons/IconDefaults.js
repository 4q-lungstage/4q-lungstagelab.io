export default name => ({
  name,
  props: {
    color: { type: String }
  }
})
