import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import AsyncComputed from 'vue-async-computed'
import i18n from './i18n'

Vue.config.productionTip = false
Vue.use(AsyncComputed)

i18n.locale = store.state.defaultLocale
i18n.fallbackLocale = store.state.defaultLocale
store.state.locale = i18n.locale

// eslint-disable-next-line
new Vue({
  el: '#app',
  components: {
    App
  },
  template: '<App/>',
  i18n,
  router,
  store
})
