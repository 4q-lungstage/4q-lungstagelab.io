module.exports = {
  splitChunks: {
    chunks: "all",
    cacheGroups: {
      commons: {
        test: /[\\/]node_modules[\\/]/,
        name: "vendor",
        chunks: "all",
      },
      app: {
        name: 'app',
        minChunks: 2,
        priority: -20,
        chunks: "all"
      }
    }
  }
}
